
FROM alpine
# Create app directorydoc

WORKDIR /usr/src/app

# Install app dependencies

# A wildcard is used to ensure both package.json AND package-lock.json are copied

# where available (npm@5+)

COPY package*.json ./

COPY server.js ./




RUN apk add --no-cache make gcc g++ python nodejs




RUN npm install

# If you are building your code for production
# RUN npm install --only=production

#COPY . .
run rm -rf /var/cache/apk/*


EXPOSE 8099

CMD [ "npm", "start" ]