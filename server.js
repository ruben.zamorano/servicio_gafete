var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var morgan = require('morgan');



// configure app
app.use(morgan('dev')); // log requests to the console

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8099; // set our port



// ROUTES FOR OUR API
// =============================================================================

// create our router
var router = express.Router();

// middleware to use for all requests
router.use(function (req, res, next) {
    // do logging
    console.log('Something is happening.');
    next();
});




// -------------------------------------------------------- //
router.route('/find_empleado')

    .post(function (req, res) {




        res.json({
            cCodigoQR: Math.floor(Math.random() * Math.floor(9)) + "" + Math.floor(Math.random() * Math.floor(9)) +  "" +Math.floor(Math.random() * Math.floor(9)) +  "" +Math.floor(Math.random() * Math.floor(9)) +  "" +Math.floor(Math.random() * Math.floor(9)), 
            cNombre:"Octavio",
            cApellido:"El pollo",
            cPuesto:"Super gerente mundial",
            nLugar1: Math.floor(Math.random() * Math.floor(9)) + "" + Math.floor(Math.random() * Math.floor(9)) + "" +Math.floor(Math.random() * Math.floor(9)) ,
            nLugar2: Math.floor(Math.random() * Math.floor(9)) + "" + Math.floor(Math.random() * Math.floor(9)) + "" +Math.floor(Math.random() * Math.floor(9)) ,
            nLugar3: Math.floor(Math.random() * Math.floor(9)) + "" + Math.floor(Math.random() * Math.floor(9)) + "" +Math.floor(Math.random() * Math.floor(9)) 

           

        });
        

    });

// -------------------------------------------------------- //







// REGISTER OUR ROUTES -------------------------------
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
